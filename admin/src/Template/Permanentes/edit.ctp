<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Permanente $permanente
 */
?>
<style>
  @import url(https://fonts.googleapis.com/icon?family=Material+Icons);
  @import url("https://fonts.googleapis.com/css?family=Raleway");
  .box {
    display: block;
    min-width: 300px;
    height: 300px;
    margin: 10px;
    background-color: white;
    border-radius: 5px;
    box-shadow: 0 1px 3px rgba(0, 0, 0, 0.12), 0 1px 2px rgba(0, 0, 0, 0.24);
    transition: all 0.3s cubic-bezier(0.25, 0.8, 0.25, 1);
    overflow: hidden;
  }
  .box-2 {
    display: block;
    min-width: 300px;
    height: 300px;
    margin: 10px;
    background-color: white;
    border-radius: 5px;
    box-shadow: 0 1px 3px rgba(0, 0, 0, 0.12), 0 1px 2px rgba(0, 0, 0, 0.24);
    transition: all 0.3s cubic-bezier(0.25, 0.8, 0.25, 1);
    overflow: hidden;
  }

  .upload-options {
    position: relative;
    height: 75px;
    background-color: cadetblue;
    cursor: pointer;
    overflow: hidden;
    text-align: center;
    transition: background-color ease-in-out 150ms;
  }
  .upload-options:hover {
    background-color: #7fb1b3;
  }
  .upload-options input {
    width: 0.1px;
    height: 0.1px;
    opacity: 0;
    overflow: hidden;
    position: absolute;
    z-index: -1;
  }
  .upload-options label {
    display: flex;
    align-items: center;
    width: 100%;
    height: 100%;
    font-weight: 400;
    text-overflow: ellipsis;
    white-space: nowrap;
    cursor: pointer;
    overflow: hidden;
  }
  .upload-options label::after {
    content: 'add';
    font-family: 'Material Icons';
    position: absolute;
    font-size: 2.5rem;
    color: #e6e6e6;
    top: calc(50% - 2.5rem);
    left: calc(50% - 1.25rem);
    z-index: 0;
  }
  .upload-options label span {
    display: inline-block;
    width: 50%;
    height: 100%;
    text-overflow: ellipsis;
    white-space: nowrap;
    overflow: hidden;
    vertical-align: middle;
    text-align: center;
  }
  .upload-options label span:hover i.material-icons {
    color: lightgray;
  }

  .js--image-preview {
    height: 225px;
    width: 100%;
    position: relative;
    overflow: hidden;
    background-image: url("");
    background-color: white;
    background-position: center center;
    background-repeat: no-repeat;
    background-size: cover;
  }
  .js--image-preview::after {
   
    position: relative;
    font-size: 4.5em;
    color: #e6e6e6;
    top: calc(50% - 3rem);
    left: calc(50% - 2.25rem);
    z-index: 0;
  }
  .js--image-preview.js--no-default::after {
    display: none;
  }
  .js--image-preview:nth-child(2) {
    background-image: url("http://bastianandre.at/giphy.gif");
  }

  i.material-icons {
    transition: color 100ms ease-in-out;
    font-size: 2.25em;
    line-height: 55px;
    color: white;
    display: block;
  }

  .drop {
    display: block;
    position: absolute;
    background: rgba(95, 158, 160, 0.2);
    border-radius: 100%;
    -webkit-transform: scale(0);
            transform: scale(0);
  }

  .animate {
    -webkit-animation: ripple 0.4s linear;
            animation: ripple 0.4s linear;
  }
  input::placeholder, textarea::placeholder{
    color:#151b1e40!important;
  }
  .jqte_tool_label{
      height:25px!important;
  }

  @-webkit-keyframes ripple {
    100% {
      opacity: 0;
      -webkit-transform: scale(2.5);
              transform: scale(2.5);
    }
  }

  @keyframes ripple {
    100% {
      opacity: 0;
      -webkit-transform: scale(2.5);
              transform: scale(2.5);
    }
  }

</style>
<ol class="breadcrumb">
    <li class="breadcrumb-item">Actividad</li>
    <li class="breadcrumb-item ">Editar</li>
    <li class="breadcrumb-item active"><?= h($permanente->titulo) ?></li>
</ol>
<?= $this->Form->create($permanente,['type' => 'file']) ?>
<div class="container">
    <div class="row">
        <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <small>1</small>
                            <strong>DETALLES</strong>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="name">Titulo</label>
                                        <?php echo $this->Form->control('titulo',['label' => false,'class'=>'form-control']);?>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label for="name">Descripción</label>
                                        <?php echo $this->Form->control('descripcion',['label' => false,'class'=>'jqte-test']);?>
                                    </div>
                                  
                                    <div class="form-group">
                                        <label for="name">Lugar</label>
                                        <?php echo $this->Form->control('lugar',['label' => false,'class'=>'form-control']);?>
                                    </div>
                                    <div class="form-group">
                                        <label for="name">Direccion</label>
                                        <?php echo $this->Form->control('direccion',['label' => false,'class'=>'form-control']);?>
                                    </div>
                                    <div class="form-group">
                                        <label for="name">Entrada</label>
                                        <?php echo $this->Form->control('entrada',['label' => false,'class'=>'form-control']);?>
                                    </div>
                                    <div class="form-group">
                                                        <label for="name">Horario</label>
                                                        <?php echo $this->Form->control('hora',['label' => false,'class'=>'form-control']);?>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label" ></label>
                                        <div class="col-sm-9">
                                            <div>
                                                <select name="activo" id="activo" >
                                                    <?php
                                                        $activo = $permanente->activo;
                                                        if ($activo==1) {
                                                            echo '<option value="1">Si</option>
                                                                  <option value="0">No</option>';
                                                        }else{
                                                            echo '<option value="0">No</option>
                                                                  <option value="1">Si</option>';
                                                        }
                                                   ?>					  
                                                </select>
                                                <label for="checkboxExample1">Activo</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label" ></label>
                                        <div class="col-sm-9">
                                            <div>
                                                <select name="destacar" id="destacar" >
                                                    <?php
                                                        $activo = $permanente->destacar;
                                                        if ($activo==1) {
                                                            echo '<option value="1">Si</option>
                                                                  <option value="0">No</option>';
                                                        }else{
                                                            echo '<option value="0">No</option>
                                                                  <option value="1">Si</option>';
                                                        }
                                                   ?>					  
                                                </select>
                                                <label for="checkboxExample1">Destacar en Lugares</label>
                                            </div>
                                        </div>
                                    </div>

                                    
                                </div>
                            </div>
                        </div>
                    </div>
        </div>
        <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <small>2</small>
                            <strong>UBICACION</strong>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-12">
                                <div class="form-group">
                                                                <div class="alert alert-success">
                                                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                                                Doble click en la ubicacion <strong>exacta</strong>  del <a href="" class="alert-link"> mapa</a>.
                                                            </div>
                                                            
                                                            <div class="col-sm-12">
                                                                <script src='https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-geocoder/v3.1.4/mapbox-gl-geocoder.min.js'></script>
         <link rel='stylesheet' href='https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-geocoder/v3.1.4/mapbox-gl-geocoder.css' type='text/css' />
                                                                  <div id='map' style='width:100%; height: 300px;'></div>
                                                                </div>
                                                            </div>
                                                            <center>
                                                            <?php
                                                            echo $this->Form->control('latitud', [
                                                                'templates' => [
                                                                    'inputContainer' => '<div class="form-group">
                                                                                                            <label class="col-sm-3 control-label" for="w4-username">Latitud</label>
                                                                                                            <div class="col-sm-9">{{content}}</div></div>'
                                                                ],
                                                                "class" => "form-control",
                                                                "",
                                                                'label' => false
                                                                ]);
                                                                echo $this->Form->control('longitud', [
                                                                'templates' => [
                                                                    'inputContainer' => '<div class="form-group">
                                                                                                            <label class="col-sm-3 control-label" for="w4-username">Longitud</label>
                                                                                                            <div class="col-sm-9">{{content}}</div></div>'
                                                                ],
                                                                "class" => "form-control",
                                                                "",
                                                                'label' => false
                                                                ]); ?>
                                                              </center>
                                                            

                                                            
                                                        <script>
                                                        mapboxgl.accessToken = 'pk.eyJ1IjoibWF1cmktdHJvdWJsZSIsImEiOiJjam9yYjN5bDQwZHpwM2ttb2pzamk1aGFxIn0.R8J8T6_cMgJ0LBUCklOEDQ';
                                                        var map = new mapboxgl.Map({
                                                            container: 'map', // container id
                                                            style: 'mapbox://styles/mapbox/streets-v9',
                                                            center: [-57.63095844715161,-25.297305326523215], // starting position
                                                            zoom: 9 // starting zoom
                                                        });
                                                    

                                                        map.on('dblclick', function (e) {
                                                            
                                                            var myJSON = JSON.stringify(e.lngLat);
                                                            var myObj = JSON.parse(myJSON);
                                                            document.getElementById("longitud").value = myObj.lng;
                                                            document.getElementById("latitud").value = myObj.lat;
                                                            document.getElementById("ubi").value =  JSON.stringify(e.lngLat);
                                                        });
                                                        map.addControl(new MapboxGeocoder({
                                                        accessToken: mapboxgl.accessToken
                                                        }));



                                                        </script>
                                                        
                                    
                                </div>
                            </div>
                        </div>
                    </div>
        </div>
        
        
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">
                    <small>3</small>
                    <strong>IMAGENES</strong>
                    
                                        
                                        
                                        
                                      
                                      
                    
                </div>
                <div class="card-body">
                    <div class="row">
                    
                            <div class="row">
                                
                                <div class="col-sm-4">
                                    <center><label for="name">Vista previa</label></center>
                                    <?php echo $this->Form->control('vistaprevia',['label' => false,'id'=>'base64','style'=>'display:none']);?>
                                    <div class="box-2">
                                            <?php 
                                            if (!empty($permanente->vistaprevia)) {
                                              echo '<div class="js--image-preview"><img id="img-min" style="height:100%; display:block; margin: auto!important;" src="'.$permanente->vistaprevia.'"></div>';

                                            }else{
                                              echo '<div class="js--image-preview"></div>';
                                            }
                                            ?>
                                        <div class="upload-options">
                                        <label class="lbl-miniatura">
                                          <input type="file" name="upload_image" id="upload_image" />
                                        </label>
                                        </div>
                                    </div>
                                    
                                </div>
                                <div class="col-sm-4">
                                    <center><label for="name">Portada</label></center>
                                    <div class="box">
                                            <?php 
                                            $foto_old= $permanente->portada;
                                            $foto_dir_old= $permanente->portada_dir;
                                            if (!empty($foto_old)) {
                                              echo '<div class="js--image-preview"><img id="img-port" style="height:100%; display:block; margin: auto!important;" src="https://linco.com.py/beta/voy/admin/'.$foto_dir_old.''.$foto_old.'"></div>';

                                            }else{
                                              echo '<div class="js--image-preview"></div>';
                                            }
                                            ?>
                                        <div class="upload-options">
                                        <label class="lbl-portada">
                                            <input type="file" id="portada" name="" class="image-upload" accept="image/*" />
                                        </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <center><label for="name">Afiche o imagen principal del evento</label></center>
                                    <div class="box">
                                            <?php 
                                            $logo_old= $permanente->afiche;
                                            $logo_dir_old= $permanente->foto_dir;
                                            if (!empty($logo_old)) {
                                              echo '<div class="js--image-preview"><img id="img-afi" style="height:100%; display:block; margin: auto!important;" src="https://linco.com.py/beta/voy/admin/'.$logo_dir_old.''.$logo_old.'"></div>';

                                            }else{
                                              echo '<div class="js--image-preview"></div>';
                                            }
                                            ?>
                                        <div class="upload-options">
                                        <label class="lbl-afiche">
                                            <input type="file" id="afiche" name="" class="image-upload" accept="image/*" />
                                        </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                      
                    </div>
                </div>
            
            </div>
        </div>
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">
                    <small>4</small>
                    <strong>FINALIZAR</strong>
                </div>
                <div class="card-body">
                    <div class="row">
                                                    
                    
                        <button class="btn btn-outline-primary btn-lg btn-block" type="submit">GUARDAR</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
</div>
<div id="uploadimageModal" class="modal" role="dialog">
	<div class="modal-dialog  modal-lg">
		<div class="modal-content">
      		<div class="modal-header">
        		<button type="button" class="close" data-dismiss="modal">&times;</button>
        		<h4 class="modal-title">Cortar imagen</h4>
      		</div>
      		<div class="modal-body">
        		<div class="row">
  					<div class="col-md-10 text-center">
						  <div id="image_demo" style="width:350px; margin-top:30px"></div>
  					</div>
  					<div class="col-md-2" style="padding-top:30px;">
  						<br />
  						<br />
  						<br/>
						  <a class="btn btn-success crop_image">Cortar</a>
					</div>
				</div>
      		</div>
      		
    	</div>
    </div>
</div>
<?= $this->Form->end() ?>
<script>  
  $(document).ready(function(){

    $image_crop = $('#image_demo').croppie({
      enableExif: true,
      viewport: {
        width:500,
        height:500,
        type:'square' //circle
      },
      boundary:{
        width:500,
        height:500
      }
    });

    $('#upload_image').on('change', function(){
      var reader = new FileReader();
      reader.onload = function (event) {
        $image_crop.croppie('bind', {
          url: event.target.result
        }).then(function(){
          console.log('jQuery bind complete');
        });
      }
      reader.readAsDataURL(this.files[0]);
      $('#uploadimageModal').modal('show');
    });

    $('.crop_image').click(function(event){
      $image_crop.croppie('result', {
        type: 'canvas',
        size: 'viewport'
      }).then(function(response){
        $('#uploadimageModal').modal('hide');
        $('#base64').val(response);
        $('#img-min').attr('src',response)
      })
    });

  });  
</script>
<script>
 $('.jqte-test').jqte();
        
        // settings of status
         var jqteStatus = true;
        
        $(document).ready(function() {
            $('#dataTables-example').DataTable({
                responsive: true
            });
        });
  $('.lbl-portada').click(function(){
      $('#portada').attr('name', 'portada');
      $('#img-port').attr('style','display:none;')
  });
  $('.lbl-miniatura').click(function(){
      $('#miniatura').attr('name', 'miniatura');
  });
  $('.lbl-afiche').click(function(){
      $('#afiche').attr('name', 'afiche');
      $('#img-afi').attr('style','display:none;')
  });

    function initImageUpload(box) {
      let uploadField = box.querySelector('.image-upload');

      uploadField.addEventListener('change', getFile);

      function getFile(e){
        let file = e.currentTarget.files[0];
        checkType(file);
      }
      
      function previewImage(file){
        let thumb = box.querySelector('.js--image-preview'),
            reader = new FileReader();

        reader.onload = function() {
          thumb.style.backgroundImage = 'url(' + reader.result + ')';
        }
        reader.readAsDataURL(file);
        thumb.className += ' js--no-default';
      }

      function checkType(file){
        let imageType = /image.*/;
        if (!file.type.match(imageType)) {
          throw 'Datei ist kein Bild';
        } else if (!file){
          throw 'Kein Bild gewählt';
        } else {
          previewImage(file);
        }
      }
      
    }

    // initialize box-scope
    var boxes = document.querySelectorAll('.box');

    for (let i = 0; i < boxes.length; i++) {
      let box = boxes[i];
      initDropEffect(box);
      initImageUpload(box);
    }



    /// drop-effect
    function initDropEffect(box){
      let area, drop, areaWidth, areaHeight, maxDistance, dropWidth, dropHeight, x, y;
      
      // get clickable area for drop effect
      area = box.querySelector('.js--image-preview');
      area.addEventListener('click', fireRipple);
      
      function fireRipple(e){
        area = e.currentTarget
        // create drop
        if(!drop){
          drop = document.createElement('span');
          drop.className = 'drop';
          this.appendChild(drop);
        }
        // reset animate class
        drop.className = 'drop';
        
        // calculate dimensions of area (longest side)
        areaWidth = getComputedStyle(this, null).getPropertyValue("width");
        areaHeight = getComputedStyle(this, null).getPropertyValue("height");
        maxDistance = Math.max(parseInt(areaWidth, 10), parseInt(areaHeight, 10));

        // set drop dimensions to fill area
        drop.style.width = maxDistance + 'px';
        drop.style.height = maxDistance + 'px';
        
        // calculate dimensions of drop
        dropWidth = getComputedStyle(this, null).getPropertyValue("width");
        dropHeight = getComputedStyle(this, null).getPropertyValue("height");
        
        // calculate relative coordinates of click
        // logic: click coordinates relative to page - parent's position relative to page - half of self height/width to make it controllable from the center
        x = e.pageX - this.offsetLeft - (parseInt(dropWidth, 10)/2);
        y = e.pageY - this.offsetTop - (parseInt(dropHeight, 10)/2) - 30;
        
        // position drop and animate
        drop.style.top = y + 'px';
        drop.style.left = x + 'px';
        drop.className += ' animate';
        e.stopPropagation();
        
      }
    }
</script>
