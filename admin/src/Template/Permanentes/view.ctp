<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Permanente $permanente
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Permanente'), ['action' => 'edit', $permanente->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Permanente'), ['action' => 'delete', $permanente->id], ['confirm' => __('Are you sure you want to delete # {0}?', $permanente->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Permanentes'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Permanente'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="permanentes view large-9 medium-8 columns content">
    <h3><?= h($permanente->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('User') ?></th>
            <td><?= $permanente->has('user') ? $this->Html->link($permanente->user->id, ['controller' => 'Users', 'action' => 'view', $permanente->user->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Titulo') ?></th>
            <td><?= h($permanente->titulo) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Lugar') ?></th>
            <td><?= h($permanente->lugar) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Direccion') ?></th>
            <td><?= h($permanente->direccion) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Miniatura') ?></th>
            <td><?= h($permanente->miniatura) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Afiche') ?></th>
            <td><?= h($permanente->afiche) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Hora') ?></th>
            <td><?= h($permanente->hora) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Portada') ?></th>
            <td><?= h($permanente->portada) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Entrada') ?></th>
            <td><?= h($permanente->entrada) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Latitud') ?></th>
            <td><?= h($permanente->latitud) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Longitud') ?></th>
            <td><?= h($permanente->longitud) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Foto Dir') ?></th>
            <td><?= h($permanente->foto_dir) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Miniatura Dir') ?></th>
            <td><?= h($permanente->miniatura_dir) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Portada Dir') ?></th>
            <td><?= h($permanente->portada_dir) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Dia Evento') ?></th>
            <td><?= h($permanente->dia_evento) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Mes Evento') ?></th>
            <td><?= h($permanente->mes_evento) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Anho Evento') ?></th>
            <td><?= h($permanente->anho_evento) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($permanente->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($permanente->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($permanente->modified) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Descripcion') ?></h4>
        <?= $this->Text->autoParagraph(h($permanente->descripcion)); ?>
    </div>
    <div class="row">
        <h4><?= __('Ubicacion') ?></h4>
        <?= $this->Text->autoParagraph(h($permanente->ubicacion)); ?>
    </div>
</div>
