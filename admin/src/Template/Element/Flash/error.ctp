<?php
if (!isset($params['escape']) || $params['escape'] !== false) {
    $message = h($message);
}

echo "<script>Swal({
    type: 'error',
    title: 'Oops...',
    text: 'Algo salio mal, revisa todos los campos y vuelve a intentarlo!'
  })</script>";
?>
