<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Evento $evento
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Evento'), ['action' => 'edit', $evento->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Evento'), ['action' => 'delete', $evento->id], ['confirm' => __('Are you sure you want to delete # {0}?', $evento->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Eventos'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Evento'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="eventos view large-9 medium-8 columns content">
    <h3><?= h($evento->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('User') ?></th>
            <td><?= $evento->has('user') ? $this->Html->link($evento->user->id, ['controller' => 'Users', 'action' => 'view', $evento->user->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Titulo') ?></th>
            <td><?= h($evento->titulo) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Lugar') ?></th>
            <td><?= h($evento->lugar) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Direccion') ?></th>
            <td><?= h($evento->direccion) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Miniatura') ?></th>
            <td><?= h($evento->miniatura) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Afiche') ?></th>
            <td><?= h($evento->afiche) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Hora') ?></th>
            <td><?= h($evento->hora) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Destacado') ?></th>
            <td><?= h($evento->destacado) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Portada') ?></th>
            <td><?= h($evento->portada) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Latitud') ?></th>
            <td><?= h($evento->latitud) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Longitud') ?></th>
            <td><?= h($evento->longitud) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Foto Dir') ?></th>
            <td><?= h($evento->foto_dir) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Miniatura Dir') ?></th>
            <td><?= h($evento->miniatura_dir) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Portada Dir') ?></th>
            <td><?= h($evento->portada_dir) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Dia Evento') ?></th>
            <td><?= h($evento->dia_evento) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Mes Evento') ?></th>
            <td><?= h($evento->mes_evento) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Año Evento') ?></th>
            <td><?= h($evento->año_evento) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($evento->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($evento->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($evento->modified) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Descripcion') ?></h4>
        <?= $this->Text->autoParagraph(h($evento->descripcion)); ?>
    </div>
    <div class="row">
        <h4><?= __('Ubicacion') ?></h4>
        <?= $this->Text->autoParagraph(h($evento->ubicacion)); ?>
    </div>
    <div class="row">
        <h4><?= __('Categoria') ?></h4>
        <?= $this->Text->autoParagraph(h($evento->categoria)); ?>
    </div>
    <div class="row">
        <h4><?= __('Entrada') ?></h4>
        <?= $this->Text->autoParagraph(h($evento->entrada)); ?>
    </div>
    <div class="row">
        <h4><?= __('Ultimo Dia') ?></h4>
        <?= $this->Text->autoParagraph(h($evento->ultimo_dia)); ?>
    </div>
    <div class="row">
        <h4><?= __('Ultimo Mes') ?></h4>
        <?= $this->Text->autoParagraph(h($evento->ultimo_mes)); ?>
    </div>
    <div class="row">
        <h4><?= __('Ultimo Año') ?></h4>
        <?= $this->Text->autoParagraph(h($evento->ultimo_año)); ?>
    </div>
</div>
