<?php

	    $nombre=$_POST["nombre"];
		$email=$_POST["email"];
		$telefono=$_POST["telefono"];
		$mensaje=$_POST["mensaje"];
		$captcha = $_POST["g-recaptcha-response"];

		$template = '
            <html xmlns="http://www.w3.org/1999/xhtml">
                <head>
                    <meta name="viewport" content="width=device-width">
                    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
                </head>

                <body bgcolor="#FFFFFF">
                    <table bgcolor="#425FA8" style="width: 100%; border-top-left-radius: 3px; border-top-right-radius: 3px;">
                        <tbody>
                            <tr>
                                <td bgcolor="#425FA8">
                                    <table style="width: 100%; padding-left: 25px; padding-right: 25px; padding-top: 5px; padding-bottom: 5px;">
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <p style="font-size: 18px; color: #FFFFFF;">  
                                                        '. $nombre . ' se ha contactado
                                                    </p>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>

                    <table bgcolor="#FAFAFA" style="width: 100%; border-left: 1px solid #F0F0F0; border-right: 1px solid #F0F0F0; border-bottom: 1px solid #F0F0F0;">
                        <tbody>
                            <tr>
                                <td>
                                    <table bgcolor="#FFFFFF" style="width: 100%; padding-left: 25px; padding-right: 25px; padding-top: 15px; padding-bottom: 15px;">
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <p style="font-size: 14px; color: #6E6E6E; margin-top: 0px; margin-bottom: 7px;"><b style="color: #6E6E6E">Nombre: </b>' . $nombre . '</p>
                                                    <p style="font-size: 14px; color: #6E6E6E; margin-top: 0px; margin-bottom: 7px;"><b style="color: #6E6E6E">E-mail: </b><a style="font-size: 14px; color: #6E6E6E;" href="mailto:' . $email . '" target="_blank">' . $email . '</a></p>
                                                    <p style="font-size: 14px; color: #6E6E6E; margin-top: 0px; margin-bottom: 7px;"><b style="color: #6E6E6E">Teléfono: </b>' . $telefono . '</p>
                                                    <p style="font-size: 14px; color: #6E6E6E; margin-top: 0px; margin-bottom: 7px;"><b style="color: #6E6E6E">Mensaje: </b>' . $mensaje . '</p>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </body>
            </html>';


		$subject = "[VOY]: contacto desde la web";
		if (!empty($nombre)) {
					if (!empty($email)) {
						if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
							  if (!empty($telefono)) {
							  		if (!empty($mensaje)) {

							  			if (!empty($captcha)) {
							  				
							  				$headers = "MIME-Version: 1.0" . "\r\n";
						        			$headers .= "Content-type: text/html; charset = UTF-8" . "\r\n";
									        mail("contacto@voy.com.py", $subject, $template, $headers);
									        echo "Su mensaje fue enviado correctamente, en breve un representante se pondrá en contacto contigo";
									        die();

							  			}else{
							  				echo "Debe marcar la verificacion";
							  			}


							  		}else{
										echo "Debe ingresar su mensaje";
									}
							  	}else{
									echo "Debe ingresar su telefono";
								}
							} else {
							  echo("Su email no es valido");
							}	
					}else{
						echo "Debe ingresar su email";
					}
		}else{
			echo "Debe ingresar su nombre";
		}	




?>