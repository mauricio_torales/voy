<?php
include '../conexion.php';

?>
<!DOCTYPE html>
<html lang="es">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta charset="utf-8">
<meta content='IE=Edge;chrome=35+' https-equiv='X-UA-Compatible'/>

<link rel="stylesheet" href="../css/bootstrap.min.css">

<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
<link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.1/assets/owl.carousel.css'>
    <link rel='stylesheet' href='css/owl-tema.css'>
    <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.0-beta.40/css/uikit.min.css'>
 
<link rel="stylesheet" href="../css/voy.css">

 
</head>
<body>
<?php

$date=date_create("");

$mes_actual = date_format($date,"m");
$year = date_format($date,"Y");



if ($mes_actual == "1") {
  $mes_actual = "Enero";
  $mes_nro = "1";

} elseif ($mes_actual == "2") {
  $mes_actual = "Febrero";
  $mes_nro = "2";
} elseif ($mes_actual == "3") {
  $mes_actual = "Marzo";
  $mes_nro = "3";
} elseif ($mes_actual == "4") {
  $mes_actual = "Abril";
  $mes_nro = "4";
} elseif ($mes_actual == "5") {
  $mes_actual = "Mayo";
  $mes_nro = "5";
} elseif ($mes_actual == "6") {
  $mes_actual = "Junio";
  $mes_nro = "6";
} elseif ($mes_actual == "7") {
  $mes_actual = "Julio";
  $mes_nro = "7";
} elseif ($mes_actual == "8") {
  $mes_actual = "Agosto";
  $mes_nro = "8";
} elseif ($mes_actual == "9") {
  $mes_actual = "Septiembre";
  $mes_nro = "9";

} elseif ($mes_actual == "10") {
  $mes_actual = "Octubre";
  $mes_nro = "10";
} elseif ($mes_actual == "11") {
  $mes_actual = "Noviembre";
  $mes_nro = "11";
} elseif ($mes_actual == "12") {
  $mes_actual = "Diciembre";
  $mes_nro = "12";

}
?>

<div id="slider">
  <figure>
    <img src="../img/eventos.png" alt>
    <img src="../img/eventos-2.png" alt>
    <img src="../img/eventos.png" alt>
    <img src="../img/eventos-2.png" alt>
    <img src="../img/eventos.png" alt>
  </figure>
</div>
<div class="container-fluid">
<div class="header" id="myHeader">
  
   
      <nav class="navbar navbar-expand-lg navbar-light ">
         <img src="../img/logo.jpg" width="250" class="d-inline-block align-top logo" alt="">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
         <center>
        <div class="collapse navbar-collapse" id="navbarNav">
          <ul class="navbar-nav">
            <li class="nav-item">
               <a class="nav-link " href="../">Inicio</a>
            </li>
            <li class="nav-item">
              <a class="nav-link disabled" href="../eventos/">Eventos </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="../directorio/">Lugares</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="../permanentes">Actividades Permanentes</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="../contacto">Contactenos</a>
              </li>
          </ul>
        </div> 
        </center>
       
      </nav>
     
  
  
</div>

<div class="content">
  <div class="categorias">
        <center>
        <button onclick="location.href = '../buscar/?categoria=galerias';" class="btn-1"> GALERÍAS DE ARTE</button>
        <button onclick="location.href = '../buscar/?categoria=teatro';" class="btn-2"> TEATRO</button>
        <button onclick="location.href = '../buscar/?categoria=musica';"  class="btn-3"> MÚSICA</button>
        <button onclick="location.href = '../buscar/?categoria=exposiciones';"  class="btn-1"> EXPOSICIONES</button>
        <button onclick="location.href = '../buscar/?categoria=ferias';" class="btn-2"> FERIAS</button>
        <button onclick="location.href = '../buscar/?categoria=cine';" class="btn-4"> FESTIVAL DE CINE</button>
        </center>
</div>
<br>
<div class="container">
  <div class="row">
     <div class="col">
            <p class="texto-inicio" style="margin-top: 10px;margin-bottom: 0px;" >Sabemos que hay ferias, exposiciones, instalaciones, charlas, talleres y cursos, festivales de música y de cine. Además exposiciones permanentes en todo el país, así como también lugares capaces de inspirar emociones, museos o sitios históricos esperando a sus visitantes. Aquí reunimos todo para que puedas informarte y disfrutarlos.</p>
              
      </div>
  </div>
</div>

<br>
    <div class="container">
      <form action="../buscar/">
        <div class="row justify-content-center">
            <div class="col-12 col-md-10 row">
                <div class="col-md-5">
                   <div class="form-group row">
                      <label for="inputPassword" class="col-sm-2 col-form-label">Lugar</label>
                      <div class="col-12 col-sm-10">
                        <input type="text" class="form-control" name="lugar" id="buscado" >
                      </div>
                    </div>
                </div>      
                <div class="col-md-6">
                  <div class="form-group row justify-content-center">
                      <label for="inputPassword" class="col-sm-3 col-form-label">Fecha</label>
                      <div class="col-12 col-sm-9 row fecha-buscado">
                        <div class="col-3 col-sm-3" style="padding-left: 0!important">
                         <select name="dia" id="inputState" class="form-control">
                            <option selected></option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>
                            <option value="13">13</option>
                            <option value="14">14</option>
                            <option value="15">15</option>
                            <option value="16">16</option>
                            <option value="17">17</option>
                            <option value="18">18</option>
                            <option value="19">19</option>
                            <option value="20">20</option>
                            <option value="21">21</option>
                            <option value="22">22</option>
                            <option value="23">23</option>
                            <option value="24">24</option>
                            <option value="25">25</option>
                            <option value="26">26</option>
                            <option value="27">27</option>
                            <option value="28">28</option>
                            <option value="29">29</option>
                            <option value="30">30</option>
                            <option value="31">31</option>
                          </select>
                          <span><i class="fas fa-sort-down"></i></span>
                        </div>
                         <div class="col-5 col-sm-5" style="padding-left: 0!important">
                         <select name="mes" id="inputState" class="form-control">
                            <option selected></option>
                           <option value="enero">enero</option>
                           <option value="febrero">febrero</option>
                           <option value="marzo">marzo</option>
                           <option value="abril">abril</option>
                           <option value="mayo">mayo</option>
                           <option value="junio">junio</option>
                           <option value="julio">julio</option>
                           <option value="agosto">agosto</option>
                           <option value="septiembre">septiembre</option>
                           <option value="octubre">octubre</option>
                           <option value="noviembre">noviembre</option>
                           <option value="diciembre">diciembre</option>
                          </select>
                          <span><i class="fas fa-sort-down"></i></span>
                        </div> 
                        <div class="col-4 col-sm-4" style="padding-right: 0!important">
                         <select name="año" id="inputState" class="form-control">
                            <option selected></option>
                             <option value="<?=$year?>"><?=$year?></option>
                             <option value="<?=$year+1?>"><?=$year+1?></option>
                              <option value="<?=$year+2?>"><?=$year+2?></option>
                          </select>
                          <span style="border:none!important"><i class="fas fa-sort-down"></i></span>
                        </div>
                      </div>
                    </div>
                  
                </div> 
                <div class="col-12 col-md-1">
                  <center>
                  <button class="btn-buscar">Buscar</button>
                  </center>
                </div>             
            </div>
        </div>
      </form>
             <?php
          $sql = "SELECT * FROM eventos where mes_evento = '$mes_actual'ORDER BY dia_evento ASC ";
            $result = $db->query($sql);
            
              if ($result->num_rows > 0) {
                 echo ' <h1 style="text-transform: uppercase;margin-top: 50px;" >'.$mes_actual.'</h1>
      <div class="container">

            <div class="row"> ';
                       
                        // output data of each row
                        while($row = $result->fetch_assoc()) {
                          if ($row["mes_evento"]== "Enero") {
                              $mes = "Ene";
                            } elseif ($row["mes_evento"] == "Febrero") {
                              $mes = "Feb";
                            } elseif ($row["mes_evento"] == "Marzo") {
                              $mes = "Mar";
                            } elseif ($row["mes_evento"] == "Abril") {
                              $mes = "Abr";
                            } elseif ($row["mes_evento"] == "Mayo") {
                              $mes = "may";
                            } elseif ($row["mes_evento"] == "Junio") {
                              $mes = "Jun";
                            } elseif ($row["mes_evento"] == "Julio") {
                              $mes = "Jul";
                            } elseif ($row["mes_evento"] == "Agosto") {
                              $mes = "Ago";
                            } elseif ($row["mes_evento"] == "Septiembre") {
                              $mes = "Sep";
                            } elseif ($row["mes_evento"] == "Octubre") {
                             $mes = "Oct";
                            } elseif ($row["mes_evento"] == "Noviembre") {
                             $mes = "Nov";
                            } elseif ($row["mes_evento"] == "Diciembre") {
                              $mes = "Dic";

                            }
                          echo'
      
                          <div class="col-sm-3 tarjeta-evento2" style= "padding-left: 0!important;">
                                  <div class="item"> <a href="../evento/?id='.$row["id"].'">
                                    <img style="width:260px; height:260px;" src="' . $row["vistaprevia"].'"> </a>
                                     <div class="informacion">
                                       <div class="row">
                                  <div class="col-sm titulo-tarjeta" style="text-transform: uppercase;" >'
                                   
                                   . $row["titulo"].
                                   
                           '        </div>
                                  <div class="col-sm fecha-tarjeta-evento">'
                                    .$row["dia_evento"].'<span style="text-transform: uppercase;" > '.$mes.' </span>'.$row["año_evento"].'
                                  </div>
                              </div>
                               <a class="ver-mas" href="../evento/?id='.$row["id"].'">Ver más...</a>
                                  </div> 

                                </div>       
                          </div>';  }
                        
                    } else {
                       
                    }

                    
                    ?>
        </div>
      </div>

<?php
if ($mes_nro == "1") {
  $mes_siguiente = "Febrero";
} elseif ($mes_nro == "2") {
  $mes_siguiente= "Marzo";
} elseif ($mes_nro == "3") {
  $mes_siguiente = "Abril";
} elseif ($mes_nro == "4") {
  $mes_siguiente= "Mayo";
} elseif ($mes_nro == "5") {
  $mes_siguiente = "Junio";
} elseif ($mes_nro == "6") {
  $mes_siguiente = "Julio";
} elseif ($mes_nro == "7") {
  $mes_siguiente = "Agosto";
} elseif ($mes_nro == "8") {
  $mes_siguiente = "Septiembre";
} elseif ($mes_nro == "9") {
  $mes_siguiente = "Octubre";
} elseif ($mes_nro == "10") {
  $mes_siguiente = "Noviembre";
} elseif ($mes_nro == "11") {
  $mes_siguiente = "Diciembre";
} elseif ($mes_nro == "12") {
  $mes_siguiente = "Enero";

}
?>


      
             <?php
          $sql = "SELECT * FROM eventos where mes_evento = '$mes_siguiente'ORDER BY dia_evento ASC ";
            $result = $db->query($sql);
              if ($result->num_rows > 0) {
                
                    echo '<h1 style="text-transform: uppercase;margin-top: 50px;" >'.$mes_siguiente.'</h1>

                      <div class="container">
                        <div class="row">';
                       
                        // output data of each row
                        while($row = $result->fetch_assoc()) {
                          if ($row["mes_evento"]== "Enero") {
                              $mes = "Ene";
                            } elseif ($row["mes_evento"] == "Febrero") {
                              $mes = "Feb";
                            } elseif ($row["mes_evento"] == "Marzo") {
                              $mes = "Mar";
                            } elseif ($row["mes_evento"] == "Abril") {
                              $mes = "Abr";
                            } elseif ($row["mes_evento"] == "Mayo") {
                              $mes = "may";
                            } elseif ($row["mes_evento"] == "Junio") {
                              $mes = "Jun";
                            } elseif ($row["mes_evento"] == "Julio") {
                              $mes = "Jul";
                            } elseif ($row["mes_evento"] == "Agosto") {
                              $mes = "Ago";
                            } elseif ($row["mes_evento"] == "Septiembre") {
                              $mes = "Sep";
                            } elseif ($row["mes_evento"] == "Octubre") {
                             $mes = "Oct";
                            } elseif ($row["mes_evento"] == "Noviembre") {
                             $mes = "Nov";
                            } elseif ($row["mes_evento"] == "Diciembre") {
                              $mes = "Dic";

                            }
                          echo'
      
                          <div class="col-sm-3 tarjeta-evento-2 " style= "padding-left: 0!important;padding-botton: 20px!important; ">
                                  <div class="item"> <a href="../evento/?id='.$row["id"].'">
                                    <img style="width:260px; height:260px;" src="' . $row["vistaprevia"].'">
                                     <div class="informacion"></a>
                                       <div class="row">
                                  <div class="col-sm titulo-tarjeta" style="text-transform: uppercase;" >'
                                   
                                   . $row["titulo"].
                                   
                           '        </div>
                                  <div class="col-sm fecha-tarjeta-evento">'
                                    .$row["dia_evento"].'<span style="text-transform: uppercase;" > '.$mes.' </span>'.$row["año_evento"].'
                                  </div>
                              </div>
                               <a class="ver-mas" href="../evento/?id='.$row["id"].'">Ver más...</a>
                                  </div> 

                                </div>       
                          </div>';  }
                        
                    } else {

                       
                    }

                    ?>
        </div>
      </div>
  <?php
if ($mes_nro == "1") {
  $mes_tres = "Marzo";
} elseif ($mes_nro == "2") {
  $mes_tres= "Abril";
} elseif ($mes_nro == "3") {
  $mes_tres = "Mayo";
} elseif ($mes_nro == "4") {
  $mes_tres= "Junio";
} elseif ($mes_nro == "5") {
  $mes_tres = "Julio";
} elseif ($mes_nro == "6") {
  $mes_tres = "Agosto";
} elseif ($mes_nro == "7") {
  $mes_tres = "Septiembre";
} elseif ($mes_nro == "8") {
  $mes_tres = "Octubre";
} elseif ($mes_nro == "9") {
  $mes_tres = "Noviembre";
} elseif ($mes_nro == "10") {
  $mes_tres = "Diciembre";
} elseif ($mes_nro == "11") {
  $mes_tres = "Enero";
} elseif ($mes_nro == "12") {
  $mes_tres = "Febrero";

}
?>


      
             <?php
          $sql = "SELECT * FROM eventos where mes_evento = '$mes_tres'ORDER BY dia_evento ASC ";
            $result = $db->query($sql);
              if ($result->num_rows > 0) {
                
                    echo '<h1 style="text-transform: uppercase;margin-top: 50px;" >'.$mes_tres.'</h1>

        <div class="container">
          <div class="row">';
                       
                        // output data of each row
                        while($row = $result->fetch_assoc()) {
                          if ($row["mes_evento"]== "Enero") {
                              $mes = "Ene";
                            } elseif ($row["mes_evento"] == "Febrero") {
                              $mes = "Feb";
                            } elseif ($row["mes_evento"] == "Marzo") {
                              $mes = "Mar";
                            } elseif ($row["mes_evento"] == "Abril") {
                              $mes = "Abr";
                            } elseif ($row["mes_evento"] == "Mayo") {
                              $mes = "may";
                            } elseif ($row["mes_evento"] == "Junio") {
                              $mes = "Jun";
                            } elseif ($row["mes_evento"] == "Julio") {
                              $mes = "Jul";
                            } elseif ($row["mes_evento"] == "Agosto") {
                              $mes = "Ago";
                            } elseif ($row["mes_evento"] == "Septiembre") {
                              $mes = "Sep";
                            } elseif ($row["mes_evento"] == "Octubre") {
                             $mes = "Oct";
                            } elseif ($row["mes_evento"] == "Noviembre") {
                             $mes = "Nov";
                            } elseif ($row["mes_evento"] == "Diciembre") {
                              $mes = "Dic";

                            }
                          echo'
      
                          <div class="col-sm-3 tarjeta-evento-2" "padding-left: 0!important;padding-botton: 20px!important; ">
                                  <div class="item"> 
                                  <a href="../evento/?id='.$row["id"].'">
                                    <img style="width:260px; height:260px;" src="' . $row["vistaprevia"].'"></a>
                                     <div class="informacion">
                                       <div class="row">
                                  <div class="col-sm titulo-tarjeta" style="text-transform: uppercase;" >'
                                   
                                   . $row["titulo"].
                                   
                           '        </div>
                                  <div class="col-sm fecha-tarjeta-evento">'
                                    .$row["dia_evento"].'<span style="text-transform: uppercase;" > '.$mes.' </span>'.$row["año_evento"].'
                                  </div>
                              </div>
                               <a class="ver-mas" href="../evento/?id='.$row["id"].'">Ver más...</a>
                                  </div> 

                                </div>       
                          </div>';  }
                        
                    } else {

                       
                    }

                    ?>
        </div>
      </div>
      <?php $db->close();?>
   </div>
</div>



<div class="footer" style="margin-top: 50px;" >
      <div class="row info">
        <div class=" col-md-2 separador">
        </div>
        <div class=" col-md-2 logos">
            <div class="row">
              <img style="width: 160px;" src="../img/logo.png">
            </div>
            <div class="row">
             <a href="https://oiledesign.com/" target="_blank"> <img style="width: 150px;" src="../img/logo-oile.png"></a>
            </div>
        </div>
        <div class=" col-md-2">
            <div class="row">
                <div class=" redes">
                    <a href="https://www.facebook.com/OileArtDesign/" target="_blank" title="Facebook">
                      <i class="fab fa-facebook-f"></i></a>
               </div>
                <div class=" redes">
                    <a href="https://www.instagram.com/oile_py/" target="_blank" title="Facebook">
                      <i class="fab fa-instagram"></i></a>
               </div>
                <div class="redes">
                  <a href="mailto:contacto@voy.com.py" target="_blank" title="Facebook">
                  <i class="far fa-envelope"></i></a>
             </div>
            </div>
        </div>

        <div class=" col-md-2 info-footer">
          <h4>Direccion</h4>

          <p>López Moreira c/ R. Colmán <br>
          Asunción</p>
          <h4>Horarios</h4>
         <p> Lunes a Viernes: 11:00 - 19:00<br>
          Sábados:         10:00 - 18:00<br>
          Domingos:        Cerrado</p>
          <h4>Teléfono:</h4>
          <p>0981 400774</p>
          <h4>Mail:</h4>
          <p>contacto@voy.com.py</p>
        </div> 
      </div>      
</div>

</DIV>
<script>
window.onscroll = function() {myFunction()};

var header = document.getElementById("myHeader");
var sticky = header.offsetTop;

function myFunction() {
  if (window.pageYOffset > sticky) {
    header.classList.add("sticky");
  } else {
    header.classList.remove("sticky");
  }
}
</script>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js'></script>   
    <script src='https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.1/owl.carousel.js'></script>
    <script src="../js/slider.js"></script>


<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
</body>
</html>
