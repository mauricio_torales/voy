<?php
include '../conexion.php';

?>

<!DOCTYPE html>
<html lang="es">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta charset="utf-8">
<meta content='IE=Edge;chrome=35+' https-equiv='X-UA-Compatible'/>

<link rel="stylesheet" href="../css/bootstrap.min.css">

<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
<link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.1/assets/owl.carousel.css'>
    <link rel='stylesheet' href='css/owl-tema.css'>
    <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.0-beta.40/css/uikit.min.css'>
 
<link rel="stylesheet" href="../css/voy.css">

<script src='https://api.tiles.mapbox.com/mapbox-gl-js/v0.51.0/mapbox-gl.js'></script>
    <link href='https://api.tiles.mapbox.com/mapbox-gl-js/v0.51.0/mapbox-gl.css' rel='stylesheet' />

 

</head>
<body>
<?php
$evento=$_GET['id'];
$sql = "SELECT * FROM eventos where id = '$evento' ";
$result = $db->query($sql);
 if ($result->num_rows > 0) {
while($row = $result->fetch_assoc()) {

echo '<div class="top-container" style="background-image: url(https://linco.com.py/beta/voy/admin/'. $row["portada_dir"].''. $row["portada"].'");"></div>';

$tag= $row["categoria"];
 ?>



<div class="container-fluid">
<div class="header" id="myHeader">
  
   
     <nav class="navbar navbar-expand-lg navbar-light ">
         <img src="../img/logo.jpg" class="d-inline-block align-top logo" alt="">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <center>
        <div class="collapse navbar-collapse" id="navbarNav">
          <ul class="navbar-nav">
            <li class="nav-item">
               <a class="nav-link" href="../">Inicio</a>
            </li>
            <li class="nav-item">
              <a class="nav-link disabled" href="../eventos/">Eventos </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="../directorio/">Lugares</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="../permanentes">Actividades Permanentes</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="../contacto">Contactenos</a>
              </li>
          </ul>
        </div> 
        </center>
      </nav>
     
  
  
</div>

<div class="content">
    <div class="container">



        
        
            <div class="row justify-content-md-center padding ">
               <div class=" col-12 col-md-6 imagen-evento">
               <?php  echo'<img width="100%" src="https://linco.com.py/beta/voy/admin/'.$row["foto_dir"].''.$row["afiche"].'">' ?>
               </div>
               <div class=" col-12 col-md-4">
                  <div class="row informacion-evento">

                    <div class="container">
                      <br>

                <?php  echo '<h2 class="titulo-evento" style="text-transform: uppercase;" >'.$row["titulo"].'</h2>' ?>

                <?php  echo  '<p>'.$row["descripcion"].'</p><br>' ?>

                      <b>Dirección:</b> <?php  echo ''.$row["direccion"].'' ?><br>



                     <?php 

                     $ultimo=$row["ultimo_dia"];

                     if ($ultimo=="") {
                        echo '<b>Fecha:</b> '.$row["dia_evento"].' de '.$row["mes_evento"].', '.$row["año_evento"].'<br>';
                     }else{
                       echo '<b>Fecha:</b> <br>Desde el '.$row["dia_evento"].' de '.$row["mes_evento"].', '.$row["año_evento"].' <br>Hasta el '.$row["ultimo_dia"].' de '.$row["ultimo_mes"].', '.$row["ultimo_año"].'<br>';

                     }

                     



                      ?>



                       <?php  
                       if (!empty($row["hora"])) {
                        echo '<b>Horario:</b>'.$row["hora"].'<br>';
                       }
                       
                        ?>
<br>
                      <center>
                        <div class="entrada">

                             <?php
                             if (!empty($row["entrada"])) {
                               echo '<span> Entrada: '.$row["entrada"].'</span>'; 
                             }?>

                        </div>
                      
                        
                      </center>


                    </div>
      
                  </div>
               </div>
            </div>
            <script type="text/javascript">
                        function mapsSelector() {
                      if /* if we're on iOS, open in Apple Maps */
                        ((navigator.platform.indexOf("iPhone") != -1) || 
                         (navigator.platform.indexOf("iPad") != -1) || 
                         (navigator.platform.indexOf("iPod") != -1))
                        window.open("https://www.google.com/maps/dir//<?=$row["latitud"]?>,<?=$row["longitud"]?>");
                    else /* else use Google */
                         window.open("https://www.google.com/maps/dir//<?=$row["latitud"]?>,<?=$row["longitud"]?>");
                    }
                  </script>
            <div class="row justify-content-md-center padding map">
              <div class="app" onclick="mapsSelector()" >
                   <p>Abrir en maps  <i class="fas fa-map-marked-alt" ></i></p>
                  </div>
                <div class="col-md-6 mapa-mobile" style="height: 400px; margin-right: 20px;">
                    <script src='https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-geocoder/v2.3.0/mapbox-gl-geocoder.min.js'></script>
                  <link rel='stylesheet' href='https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-geocoder/v2.3.0/mapbox-gl-geocoder.css' type='text/css' />
                  <div id='map'></div>

        <?php echo '<script>
                    mapboxgl.accessToken = "pk.eyJ1IjoibWF1cmktdHJvdWJsZSIsImEiOiJjam9yYjN5bDQwZHpwM2ttb2pzamk1aGFxIn0.R8J8T6_cMgJ0LBUCklOEDQ";

                    /* Map: This represents the map on the page. */
                    var map = new mapboxgl.Map({
                      container: "map",
                      style: "mapbox://styles/mapbox/basic-v9",
                      zoom:15,
                      center: ['.$row["longitud"].','.$row["latitud"].']
                    });

                    map.on("load", function () {
                      /* Image: An image is loaded and added to the map. */
                      map.loadImage("../img/marcador.png", function(error, image) {
                          if (error) throw error;
                          map.addImage("custom-marker", image);
                          /* Style layer: A style layer ties together the source and image and specifies how they are displayed on the map. */
                          map.addLayer({
                            id: "markers",
                            type: "symbol",
                            /* Source: A data source specifies the geographic coordinate where the image marker gets placed. */
                            source: {
                              type: "geojson",
                              data: {
                                type: "FeatureCollection",
                                features:[{"type":"Feature","geometry":{"type":"Point","coordinates":['.$row["longitud"].','.$row["latitud"].']}}]}
                            },
                            layout: {
                              "icon-image": "custom-marker",
                            }
                          });
                        });
                    });
                      </script>';?>
                  

                </div>
                 <div class="col-xs-12 col-md-4 publicidad-slider" >
                  <div id="slider-publi">
                    <figure>
                      <a href="https://www.senatur.gov.py/"> <img src="../img/publi/SENATUR.png" alt></a>
                     <a href="https://oiledesign.com/"> <img src="../img/publi/oile2.jpg" alt></a>
                      <a href="https://www.senatur.gov.py/"> <img src="../img/publi/SENATUR.png" alt></a>
                      <a href="https://oiledesign.com/"> <img src="../img/publi/oile.jpg" alt></a>
                     <a href="https://www.senatur.gov.py/"> <img src="../img/publi/SENATUR.png" alt></a>
                    </figure>
                  </div>
                </div>
                 <div class="col-xs-12 col-md-12" >
                    <center><a href="../eventos/"><img class="volver" width="100" src="../img/volver.svg" alt="volver"></a></center>
                </div>

            </div>
          

<?php
}
}else {
                        
}

?>




   <h1> EVENTOS RELACIONADOS</h1>
    <div class="uk-section">
        <div class="owl-carousel owl-theme">
<?php

$date=date_create("");

$mes_actual = date_format($date,"M");



if ($mes_actual == "Jan") {
  $mes_actual = "Enero";
} elseif ($mes_actual == "Feb") {
  $mes_actual = "Febrero";
} elseif ($mes_actual == "Mar") {
  $mes_actual = "Marzo";
} elseif ($mes_actual == "Apr") {
  $mes_actual = "Abril";
} elseif ($mes_actual == "May") {
  $mes_actual = "Mayo";
} elseif ($mes_actual == "Jun") {
  $mes_actual = "Junio";
} elseif ($mes_actual == "Jul") {
  $mes_actual = "Julio";
} elseif ($mes_actual == "Aug") {
  $mes_actual = "Agosto";
} elseif ($mes_actual == "Sep") {
  $mes_actual = "Septiembre";
} elseif ($mes_actual == "Oct") {
  $mes_actual = "Octubre";
} elseif ($mes_actual == "Nov") {
  $mes_actual = "Noviembre";
} elseif ($mes_actual == "Dec") {
  $mes_actual = "Diciembre";

}

          $sql = "SELECT * FROM eventos where categoria = '$tag' and id <> '$evento' ";
            $result = $db->query($sql);
              if ($result->num_rows > 0) {
                       
                        // output data of each row
                        while($row = $result->fetch_assoc()) {
                          if ($row["mes_evento"]== "Enero") {
                              $mes = "Ene";
                            } elseif ($row["mes_evento"] == "Febrero") {
                              $mes = "Feb";
                            } elseif ($row["mes_evento"] == "Marzo") {
                              $mes = "Mar";
                            } elseif ($row["mes_evento"] == "Abril") {
                              $mes = "Abr";
                            } elseif ($row["mes_evento"] == "Mayo") {
                              $mes = "may";
                            } elseif ($row["mes_evento"] == "Junio") {
                              $mes = "Jun";
                            } elseif ($row["mes_evento"] == "Julio") {
                              $mes = "Jul";
                            } elseif ($row["mes_evento"] == "Agosto") {
                              $mes = "Ago";
                            } elseif ($row["mes_evento"] == "Septiembre") {
                              $mes = "Sep";
                            } elseif ($row["mes_evento"] == "Octubre") {
                             $mes = "Oct";
                            } elseif ($row["mes_evento"] == "Noviembre") {
                             $mes = "Nov";
                            } elseif ($row["mes_evento"] == "Diciembre") {
                              $mes = "Dic";

                            }
                         
                         

                         echo'  <div class="item"> 
                            <img style="width:260px; height:260px;" src="'. $row["vistaprevia"].'">
                             <div class="informacion">
                               <div class="row">
                                  <div class="col-sm titulo-tarjeta" style="text-transform: uppercase;" >'
                                   
                                   . $row["titulo"].
                                   
                           '        </div>
                                  <div class="col-sm fecha-tarjeta-evento">'
                                    .$row["dia_evento"].'<span style="text-transform: uppercase;" > '.$mes.' </span>'.$row["año_evento"].'
                                  </div>
                              </div>
                               <a class="ver-mas"href="../evento/?id='.$row["id"].'">Ver más...</a>
                             </div>     
                          </div>';




                          
                        }
                        
                    } else {
                       
                    }
                    $db->close();
                    ?>





            
            
            
            
            
            
            
        </div>
    </div>


   </div>
</div>


<div class="footer" >
      <div class="row info">
        <div class=" col-md-2 separador">
        </div>
        <div class=" col-md-2 logos">
            <div class="row">
              <img style="width: 160px;" src="../img/logo.png">
            </div>
            <div class="row">
             <a href="https://oiledesign.com/" target="_blank"> <img style="width: 150px;" src="../img/logo-oile.png"></a>
            </div>
        </div>
        <div class=" col-md-2">
            <div class="row">
                <div class=" redes">
                    <a href="https://www.facebook.com/OileArtDesign/" target="_blank" title="Facebook">
                      <i class="fab fa-facebook-f"></i></a>
               </div>
                <div class=" redes">
                    <a href="https://www.instagram.com/oile_py/" target="_blank" title="Facebook">
                      <i class="fab fa-instagram"></i></a>
               </div>
                <div class="redes">
                  <a href="mailto:contacto@voy.com.py" target="_blank" title="Facebook">
                  <i class="far fa-envelope"></i></a>
             </div>
            </div>
        </div>

        <div class=" col-md-2 info-footer">
          <h4>Direccion</h4>

          <p>López Moreira c/ R. Colmán <br>
          Asunción</p>
          <h4>Horarios</h4>
         <p> Lunes a Viernes: 11:00 - 19:00<br>
          Sábados:         10:00 - 18:00<br>
          Domingos:        Cerrado</p>
          <h4>Teléfono:</h4>
          <p>0981 400774</p>
          <h4>Mail:</h4>
          <p>contacto@voy.com.py</p>
        </div> 
      </div>      
</div>
<script>
window.onscroll = function() {myFunction()};

var header = document.getElementById("myHeader");
var sticky = header.offsetTop;

function myFunction() {
  if (window.pageYOffset > sticky) {
    header.classList.add("sticky");
  } else {
    header.classList.remove("sticky");
  }
}
</script>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js'></script>   
    <script src='https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.1/owl.carousel.js'></script>
    <script src="../js/slider-2.js"></script>


<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
</body>
</html>
