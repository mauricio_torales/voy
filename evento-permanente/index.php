<?php
include '../conexion.php';

?>

<!DOCTYPE html>
<html lang="es">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta charset="utf-8">
<meta content='IE=Edge;chrome=35+' https-equiv='X-UA-Compatible'/>

<link rel="stylesheet" href="../css/bootstrap.min.css">

<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
<link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.1/assets/owl.carousel.css'>
    <link rel='stylesheet' href='../css/owl-tema.css'>
    <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.0-beta.40/css/uikit.min.css'>
 
<link rel="stylesheet" href="../css/voy.css">

<script src='https://api.tiles.mapbox.com/mapbox-gl-js/v0.51.0/mapbox-gl.js'></script>
    <link href='https://api.tiles.mapbox.com/mapbox-gl-js/v0.51.0/mapbox-gl.css' rel='stylesheet' />

 

</head>
<body>
<?php
$eventos=$_GET['id'];
$sql = "SELECT * FROM permanentes where id = '$eventos' ";
$result = $db->query($sql);
 if ($result->num_rows > 0) {
while($row = $result->fetch_assoc()) {

echo '<div class="top-container" style="background-image: url(https://linco.com.py/beta/voy/admin/'. $row["portada_dir"].''. $row["portada"].'");"></div>';
 ?>
<div class="container-fluid">
<div class="header" id="myHeader">
  
   
     <nav class="navbar navbar-expand-lg navbar-light ">
         <img src="../img/logo.jpg" class="d-inline-block align-top logo" alt="">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
         <center>
        <div class="collapse navbar-collapse" id="navbarNav">
          <ul class="navbar-nav">
            <li class="nav-item">
               <a class="nav-link" href="../">Inicio</a>
            </li>
            <li class="nav-item">
              <a class="nav-link " href="../eventos/">Eventos </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="../directorio/">Lugares</a>
            </li>
            <li class="nav-item">
              <a class="nav-link disabled" href="../permanentes">Actividades Permanentes</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="../contacto">Contactenos</a>
              </li>
          </ul>
        </div> 
          </center>
      </nav>

  
</div>

<div class="content">
    <div class="container">
        
        
            <div class="row justify-content-md-center padding ">
              <?php if (!empty($row["afiche"])) { ?>
                <div class="col-md-6 imagen-evento">
                  <?php  echo'<img src="https://linco.com.py/beta/voy/admin/'.$row["foto_dir"].''.$row["afiche"].'">' ?>
                </div>
               <div class="col-md-4">
                  <div class="row informacion-evento">

                    <div class="container">
                      <br>
                       <?php  echo '<h2 style="text-transform: uppercase;" >'.$row["titulo"].'</h2>' ?>
                      <br>
                      <b>Lugar:</b> <?php  echo ''.$row["lugar"].'' ?><br><br>
                      <b>Dirección:</b> <?php  echo ''.$row["direccion"].'' ?><br><br>

                       <?php
                       $dia=$row["dia_evento"];
                       if ( $dia =="") {
                        
                       }else{
                         echo '<b>Fecha:</b> '.$row["dia_evento"].' de '.$row["mes_evento"].', '.$row["año_evento"].'<br><br>';
                         };
                         ?>


                       <?php  echo '<b>Horarios:</b> '.$row["hora"].'' ?>
                       <?php  echo  '<p>'.$row["descripcion"].'</p><br>' ?>
                      <center>
                         <div class="entrada">
                        <span> Entrada <?php  echo ''.$row["entrada"].'' ?></span>
                          </div>
                      </center>

                    </div>
      
                  </div>
               </div>

              <?php }else{ ?>
                <div class="col-md-10 ">
                  <div class="row informacion-evento">

                    <div class="container">
                      <br>
                       <?php  echo '<h2 style="text-transform: uppercase;" >'.$row["titulo"].'</h2>' ?>
                      <br>
                      <b>Lugar:</b> <?php  echo ''.$row["lugar"].'' ?><br><br>
                      <b>Dirección:</b> <?php  echo ''.$row["direccion"].'' ?><br><br>

                       <?php
                       $dia=$row["dia_evento"];
                       if ( $dia =="") {
                        
                       }else{
                         echo '<b>Fecha:</b> '.$row["dia_evento"].' de '.$row["mes_evento"].', '.$row["año_evento"].'<br><br>';
                         };
                         ?>


                       <?php  echo '<b>Horarios:</b> '.$row["hora"].'' ?>
                       <?php  echo  '<p>'.$row["descripcion"].'</p><br>' ?>
                      <center>
                         <div class="entrada">
                        <span> Entrada <?php  echo ''.$row["entrada"].'' ?></span>
                          </div>
                      </center>

                    </div>
      
                  </div>
               </div>
              <?php } ?>
               
            </div>
                  <script type="text/javascript">
                        function mapsSelector() {
                      if /* if we're on iOS, open in Apple Maps */
                        ((navigator.platform.indexOf("iPhone") != -1) || 
                         (navigator.platform.indexOf("iPad") != -1) || 
                         (navigator.platform.indexOf("iPod") != -1))
                        window.open("https://www.google.com/maps/dir//<?=$row["latitud"]?>,<?=$row["longitud"]?>");
                    else /* else use Google */
                         window.open("https://www.google.com/maps/dir//<?=$row["latitud"]?>,<?=$row["longitud"]?>");
                    }
                  </script>
                    
                  
            <div class="row justify-content-md-center padding ">
              <div class="app" onclick="mapsSelector()" >
                   <p>Abrir en maps  <i class="fas fa-map-marked-alt" ></i></p>
                  </div>

               <div class="col-md-6 mapa-mobile" style="height: 400px;margin-right: 20px;">
                    <script src='https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-geocoder/v2.3.0/mapbox-gl-geocoder.min.js'></script>
                  <link rel='stylesheet' href='https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-geocoder/v2.3.0/mapbox-gl-geocoder.css' type='text/css' />

                  <div id='map'></div>
                  <?php echo '<script>
                    mapboxgl.accessToken = "pk.eyJ1IjoibWF1cmktdHJvdWJsZSIsImEiOiJjam9yYjN5bDQwZHpwM2ttb2pzamk1aGFxIn0.R8J8T6_cMgJ0LBUCklOEDQ";

                    /* Map: This represents the map on the page. */
                    var map = new mapboxgl.Map({
                      container: "map",
                      style: "mapbox://styles/mapbox/basic-v9",
                      zoom:15,
                      center: ['.$row["longitud"].','.$row["latitud"].']
                    });

                    map.on("load", function () {
                      /* Image: An image is loaded and added to the map. */
                      map.loadImage("../img/marcador.png", function(error, image) {
                          if (error) throw error;
                          map.addImage("custom-marker", image);
                          /* Style layer: A style layer ties together the source and image and specifies how they are displayed on the map. */
                          map.addLayer({
                            id: "markers",
                            type: "symbol",
                            /* Source: A data source specifies the geographic coordinate where the image marker gets placed. */
                            source: {
                              type: "geojson",
                              data: {
                                type: "FeatureCollection",
                                features:[{"type":"Feature","geometry":{"type":"Point","coordinates":['.$row["longitud"].','.$row["latitud"].']}}]}
                            },
                            layout: {
                              "icon-image": "custom-marker",
                            }
                          });
                        });
                    });
                      </script>';?>

                </div>
                 <div class="col-md-4 publicidad-slider"  style="height: 400px;">

                 
                  <div id="slider-publi">
                    <figure>
                     <a href="https://www.senatur.gov.py/"> <img src="../img/publi/SENATUR.png" alt></a>
                     <a href="https://oiledesign.com/"> <img src="../img/publi/oile2.jpg" alt></a>
                      <a href="https://www.senatur.gov.py/"> <img src="../img/publi/SENATUR.png" alt></a>
                      <a href="https://oiledesign.com/"> <img src="../img/publi/oile.jpg" alt></a>
                     <a href="https://www.senatur.gov.py/"> <img src="../img/publi/SENATUR.png" alt></a>
                    </figure>
                  </div>
                </div>
                 <div class="col-xs-12 col-md-12" >
                    <center><a href="../permanentes/"><img class="volver" width="100" src="../img/volver.svg" alt="volver"></a></center>
                </div>
            </div>
<?php
}
}else {
                        
}
$db->close();
?>

   </div>
</div>



<div class="footer" >
      <div class="row info">
        <div class=" col-md-2 separador">
        </div>
        <div class=" col-md-2 logos">
            <div class="row">
              <img style="width: 160px;" src="../img/logo.png">
            </div>
            <div class="row">
             <a href="https://oiledesign.com/" target="_blank"> <img style="width: 150px;" src="../img/logo-oile.png"></a>
            </div>
        </div>
        <div class=" col-md-2">
            <div class="row">
                <div class=" redes">
                    <a href="https://www.facebook.com/OileArtDesign/" target="_blank" title="Facebook">
                      <i class="fab fa-facebook-f"></i></a>
               </div>
                <div class=" redes">
                    <a href="https://www.instagram.com/oile_py/" target="_blank" title="Facebook">
                      <i class="fab fa-instagram"></i></a>
               </div>
                <div class="redes">
                  <a href="mailto:contacto@voy.com.py" target="_blank" title="Facebook">
                  <i class="far fa-envelope"></i></a>
             </div>
            </div>
        </div>

       <div class=" col-md-2 info-footer">
          <h4>Direccion</h4>

          <p>López Moreira c/ R. Colmán <br>
          Asunción</p>
          <h4>Horarios</h4>
         <p> Lunes a Viernes: 11:00 - 19:00<br>
          Sábados:         10:00 - 18:00<br>
          Domingos:        Cerrado</p>
          <h4>Teléfono:</h4>
          <p>0981 400774</p>
          <h4>Mail:</h4>
          <p>contacto@voy.com.py</p>
        </div> 
      </div>      
</div>
</div>
<script>
window.onscroll = function() {myFunction()};

var header = document.getElementById("myHeader");
var sticky = header.offsetTop;

function myFunction() {
  if (window.pageYOffset > sticky) {
    header.classList.add("sticky");
  } else {
    header.classList.remove("sticky");
  }
}
</script>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js'></script>   
    <script src='https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.1/owl.carousel.js'></script>
    <script src="../js/slider.js"></script>


<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
</body>
</html>
