<?php
include '../conexion.php';

?>
<!DOCTYPE html> 
<html lang="es">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta charset="utf-8">
<meta content='IE=Edge;chrome=35+' https-equiv='X-UA-Compatible'/>

<link rel="stylesheet" href="../css/bootstrap.min.css">

<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
<link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.1/assets/owl.carousel.css'>
    <link rel='stylesheet' href='../css/owl-tema.css'>
    <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.0-beta.40/css/uikit.min.css'>
 
<link rel="stylesheet" href="../css/voy.css">

 

</head>
<body>

<div id="slider">
  <figure>
    <img src="../img/7.png" alt>
    <img src="../img/lugar.png" alt>
    <img src="../img/7.png" alt>
    <img src="../img/lugar.png" alt>
    <img src="../img/7.png" alt>
  </figure>
</div>
<div class="container-fluid">
<div class="header" id="myHeader">
  
   
      <nav class="navbar navbar-expand-lg navbar-light ">
         <img src="../img/logo.jpg" class="d-inline-block align-top logo" alt="">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
       <center>
        <div class="collapse navbar-collapse" id="navbarNav">
          <ul class="navbar-nav">
            <li class="nav-item">
               <a class="nav-link" href="../">Inicio</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="../eventos/">Eventos </a>
            </li>
            <li class="nav-item">
              <a class="nav-link disabled" href="../directorio/">Lugares</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="../permanentes">Actividades Permanentes</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="../contacto">Contactenos</a>
              </li>
          </ul>
        </div> 
         </center>
      </nav>
     
 
  
</div>

<div class="content">
  <br>
  <div class="container">

  <div class="row">
     <div class="col">
            <center>
             <p class="texto-inicio" style="margin-top: 10px;margin-bottom: 0px;" > Desde aquí podes buscar un lugar específico que deseas conocer o saber que tiene de nuevo para
mostrarte.!</p>
             </center>
      </div>
  </div>
  <br>
  <form action="../buscar/">
        <div class="row justify-content-center">
            <div class="col-12 col-md-10 row">
                <div class="col-md-5">
                   <div class="form-group row">
                      <label for="inputPassword" class="col-sm-2 col-form-label">Lugar</label>
                      <div class="col-12 col-sm-10">
                        <input type="text" class="form-control" name="lugar" id="buscado" >
                      </div>
                    </div>
                </div>      
                <div class="col-md-6">
                  <div class="form-group row justify-content-center">
                      <label for="inputPassword" class="col-sm-3 col-form-label">Fecha</label>
                      <div class="col-12 col-sm-9 row fecha-buscado">
                        <div class="col-3 col-sm-3" style="padding-left: 0!important">
                         <select name="dia" id="inputState" class="form-control">
                            <option selected></option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>
                            <option value="13">13</option>
                            <option value="14">14</option>
                            <option value="15">15</option>
                            <option value="16">16</option>
                            <option value="17">17</option>
                            <option value="18">18</option>
                            <option value="19">19</option>
                            <option value="20">20</option>
                            <option value="21">21</option>
                            <option value="22">22</option>
                            <option value="23">23</option>
                            <option value="24">24</option>
                            <option value="25">25</option>
                            <option value="26">26</option>
                            <option value="27">27</option>
                            <option value="28">28</option>
                            <option value="29">29</option>
                            <option value="30">30</option>
                            <option value="31">31</option>
                          </select>
                          <span><i class="fas fa-sort-down"></i></span>
                        </div>
                         <div class="col-5 col-sm-5" style="padding-left: 0!important">
                         <select name="mes" id="inputState" class="form-control">
                            <option selected></option>
                           <option value="enero">enero</option>
                           <option value="febrero">febrero</option>
                           <option value="marzo">marzo</option>
                           <option value="abril">abril</option>
                           <option value="mayo">mayo</option>
                           <option value="junio">junio</option>
                           <option value="julio">julio</option>
                           <option value="agosto">agosto</option>
                           <option value="septiembre">septiembre</option>
                           <option value="octubre">octubre</option>
                           <option value="noviembre">noviembre</option>
                           <option value="diciembre">diciembre</option>
                          </select>
                          <span><i class="fas fa-sort-down"></i></span>
                        </div> 
                        <div class="col-4 col-sm-4" style="padding-right: 0!important">
                         <select name="año" id="inputState" class="form-control">
                            <option selected></option>
                             <option value="<?=$year?>"><?=$year?></option>
                             <option value="<?=$year+1?>"><?=$year+1?></option>
                              <option value="<?=$year+2?>"><?=$year+2?></option>
                          </select>
                          <span style="border:none!important"><i class="fas fa-sort-down"></i></span>
                        </div>
                      </div>
                    </div>
                  
                </div> 
                <div class="col-12 col-md-1">
                  <center>
                  <button class="btn-buscar">Buscar</button>
                  </center>
                </div>             
            </div>
        </div>
      </form>
</div>
  <div class="container">

<?php

$date=date_create("");

$mes_actual = date_format($date,"M");



if ($mes_actual == "Jan") {
  $mes_actual = "Enero";
} elseif ($mes_actual == "Feb") {
  $mes_actual = "Febrero";
} elseif ($mes_actual == "Mar") {
  $mes_actual = "Marzo";
} elseif ($mes_actual == "Apr") {
  $mes_actual = "Abril";
} elseif ($mes_actual == "May") {
  $mes_actual = "Mayo";
} elseif ($mes_actual == "Jun") {
  $mes_actual = "Junio";
} elseif ($mes_actual == "Jul") {
  $mes_actual = "Julio";
} elseif ($mes_actual == "Aug") {
  $mes_actual = "Agosto";
} elseif ($mes_actual == "Sep") {
  $mes_actual = "Septiembre";
} elseif ($mes_actual == "Oct") {
  $mes_actual = "Octubre";
} elseif ($mes_actual == "Nov") {
  $mes_actual = "Noviembre";
} elseif ($mes_actual == "Dec") {
  $mes_actual = "Diciembre";

}
?>
        
         <?php
          $sql = "SELECT DISTINCT lugar FROM eventos ORDER BY lugar ASC";
            $result = $db->query($sql);
              if ($result->num_rows > 0) {
                       
                        // output data of each row
                        while($row = $result->fetch_assoc()) {
                        $sitio=$row["lugar"];  

                      echo'<h1 style="text-transform: uppercase; padding-top: 20px;" >'.$sitio.'</h1>'; 
                      echo'  <div class="uk-section">
                                <div class="owl-carousel owl-theme">';

                                $sql = "SELECT * FROM permanentes where lugar = '$sitio' and destacar=1";
                                $res = $db->query($sql);
                                  if ($res->num_rows > 0) {
                                           
                                            // output data of each row
                                            while($row = $res->fetch_assoc()) {

                                      echo'          <div class="col-sm-3 tarjeta-evento2">
                                                      <div class="item"> 
                                                      <a href="../evento-permanente/?id='.$row["id"].'">
                                                        <figure class="permanentes-vp">
                                                          <img src="' . $row["vistaprevia"].'">
                                                        </figure>
                                                        </a>
                                                         <div style="width:250px;">
                                                           <div class="row">
                                                              <div class="col-sm-12 titulo-tarjeta" style="text-transform: uppercase;" >
                                                               
                                                                '. $row["titulo"].'
                                                               
                                                              </div>
                                                             
                                                         </div>
                                                          <a href="../evento-permanente/?id='.$row["id"].'">Ver más...</a>
                                                      </div> 
                                                    </div>       
                                              </div>';
                                               }} else {}


                      
                      $sql = "SELECT * FROM eventos where lugar = '$sitio' order by mes_evento asc, dia_evento asc";
                        $resultado = $db->query($sql);
                          if ($resultado->num_rows > 0) {

                       
                        // output data of each row
                        while($col= $resultado->fetch_assoc()) {

                           if ($col["mes_evento"]== "Enero") {
                              $mes = "Ene";
                            } elseif ($col["mes_evento"] == "Febrero") {
                              $mes = "Feb";
                            } elseif ($col["mes_evento"] == "Marzo") {
                              $mes = "Mar";
                            } elseif ($col["mes_evento"] == "Abril") {
                              $mes = "Abr";
                            } elseif ($col["mes_evento"] == "Mayo") {
                              $mes = "may";
                            } elseif ($col["mes_evento"] == "Junio") {
                              $mes = "Jun";
                            } elseif ($col["mes_evento"] == "Julio") {
                              $mes = "Jul";
                            } elseif ($col["mes_evento"] == "Agosto") {
                              $mes = "Ago";
                            } elseif ($col["mes_evento"] == "Septiembre") {
                              $mes = "Sep";
                            } elseif ($col["mes_evento"] == "Octubre") {
                             $mes = "Oct";
                            } elseif ($col["mes_evento"] == "Noviembre") {
                             $mes = "Nov";
                            } elseif ($col["mes_evento"] == "Diciembre") {
                              $mes = "Dic";

                            };

                          echo' 


                                    <div class="item tarjeta-evento2"> 
                                      <img style="width:260px; height:260px;" src="' . $col["vistaprevia"].'">
                                       <div class="informacion">
                                         <div class="row">
                                            <div class="col-sm titulo-tarjeta" style="text-transform: uppercase;">
                                             
                                             '. $col["titulo"].'
                                             
                                            </div>
                                            <div class="col-sm fecha-tarjeta-evento">
                                              '. $col["dia_evento"].'<span style="text-transform: uppercase;" > '.$mes.' </span>'. $col["año_evento"].'
                                            </div>
                                        </div>
                                        <a class="ver-mas" href="../evento/?id='.$col["id"].'">Ver más...</a>
                                       </div>     
                                    </div> 
                                
                           ';}} else {}


                            echo' </div> </div>';





              }} else {}$db->close();?>




   </div>
</div>



<div class="footer" >
      <div class="row info">
        <div class=" col-md-2 separador">
        </div>
        <div class=" col-md-2 logos">
            <div class="row">
              <img style="width: 160px;" src="../img/logo.png">
            </div>
            <div class="row">
             <a href="https://oiledesign.com/" target="_blank"> <img style="width: 150px;" src="../img/logo-oile.png"></a>
            </div>
        </div>
        <div class=" col-md-2">
            <div class="row">
                <div class=" redes">
                    <a href="https://www.facebook.com/OileArtDesign/" target="_blank" title="Facebook">
                      <i class="fab fa-facebook-f"></i></a>
               </div>
                <div class=" redes">
                    <a href="https://www.instagram.com/oile_py/" target="_blank" title="Facebook">
                      <i class="fab fa-instagram"></i></a>
               </div>
                <div class="redes">
                  <a href="mailto:contacto@voy.com.py" target="_blank" title="Facebook">
                  <i class="far fa-envelope"></i></a>
             </div>
            </div>
        </div>

        <div class=" col-md-2 info-footer">
          <h4>Direccion</h4>

          <p>López Moreira c/ R. Colmán <br>
          Asunción</p>
          <h4>Horarios</h4>
         <p> Lunes a Viernes: 11:00 - 19:00<br>
          Sábados:         10:00 - 18:00<br>
          Domingos:        Cerrado</p>
          <h4>Teléfono:</h4>
          <p>0981 400774</p>
          <h4>Mail:</h4>
          <p>contacto@voy.com.py</p>
        </div> 
      </div>      
</div>
</div>
<script>
window.onscroll = function() {myFunction()};

var header = document.getElementById("myHeader");
var sticky = header.offsetTop;

function myFunction() {
  if (window.pageYOffset > sticky) {
    header.classList.add("sticky");
  } else {
    header.classList.remove("sticky");
  }
}
</script>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js'></script>   
    <script src='https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.1/owl.carousel.js'></script>
    <script src="../js/slider-2.js"></script>


<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
</body>
</html>
