<?php
include 'conexion.php';
header("Content-Type: text/html;charset=utf-8");
?>
<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta charset="utf-8">
<meta name="description" content="Creamos este calendario, donde vas a encontrar todo tipo de eventos con sus datos más importantes. Lo actualizamos mes a mes para que nada quede afuera." />
    <meta name="keywords" content="Arte, Cultura, Agenda, Expectaculos, Cine, eventos " />
    <meta name="author" content="Linco" />
<meta content='IE=Edge;chrome=35+' https-equiv='X-UA-Compatible'/>
<link rel="icon" type="image/png" href="favicon.png" />
<link rel="stylesheet" href="css/bootstrap.min.css">
<title>VOY Quehacer cultural</title>
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
<link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.1/assets/owl.carousel.css'>
<link rel='stylesheet' href='css/owl-tema.css'>
<link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.0-beta.40/css/uikit.min.css'>
 
<link rel="stylesheet" href="css/voy.css">

 

</head>
<body>

<div id="slider">
  <figure>
    <img src="img/5-2.png" alt>
    <img src="img/3-2.png" alt>
    <img src="img/5-2.png" alt>
    <img src="img/3-2.png" alt>
    <img src="img/5-2.png" alt>
  </figure>
</div>
  


<div class="container-fluid">
<div class="header" id="myHeader">
      <nav class="navbar navbar-expand-lg navbar-light ">
         <img src="img/logo.jpg" width="250" class="d-inline-block align-top logo" alt="">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <center>
        <div class="collapse navbar-collapse" id="navbarNav">
          <ul class="navbar-nav">
            <li class="nav-item">
               <a class="nav-link disabled" href="#">Inicio</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="eventos/">Eventos </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="directorio/">Lugares</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="permanentes">Actividades Permanentes</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="contacto">Contactenos</a>
              </li>
          </ul>
        </div> 
        </center>
      </nav>

  
</div>

<div class="content">
    <div class="container">

        <div class="row">

          <div class="col">
              <p class="text-center frase"><em> "El arte no cambia nada, el arte te cambia a tí" - David Lynch</em></p>
              <p class="text-center frase-2"><em> "El arte no cambia nada, el arte te cambia a tí"<br> David Lynch</em></p>
              
          </div>
        </div>
        <div class="row texto-inicio">

          <div class="col">
              <p class="text-sm-left" style="margin-top: 10px;margin-bottom: 0px;" >El arte es algo vivo, se mueve y va generando espacios donde podemos compartir pasiones e intenciones.</p>
              <p class="text-sm-left" style="margin-top: 10px;margin-bottom: 0px;" >Creamos este calendario donde vas a encontrar los datos de todo tipo de eventos y actividades culturales para que puedas conocer más de nuestros artistas, los que hacen Lo actualizamos mes a mes para que nada quede afuera.</p>
             
          </div>

        </div>

<?php

$date=date_create("");

$mes_actual = date_format($date,"M");
$dia_actual = date_format($date,"d");

if ($mes_actual == "Jan") {
  $mes_actual = "Enero";
} elseif ($mes_actual == "Feb") {
  $mes_actual = "Febrero";
} elseif ($mes_actual == "Mar") {
  $mes_actual = "Marzo";
} elseif ($mes_actual == "Apr") {
  $mes_actual = "Abril";
} elseif ($mes_actual == "May") {
  $mes_actual = "Mayo";
} elseif ($mes_actual == "Jun") {
  $mes_actual = "Junio";
} elseif ($mes_actual == "Jul") {
  $mes_actual = "Julio";
} elseif ($mes_actual == "Aug") {
  $mes_actual = "Agosto";
} elseif ($mes_actual == "Sep") {
  $mes_actual = "Septiembre";
} elseif ($mes_actual == "Oct") {
  $mes_actual = "Octubre";
} elseif ($mes_actual == "Nov") {
  $mes_actual = "Noviembre";
} elseif ($mes_actual == "Dec") {
  $mes_actual = "Diciembre";

}

//elimina todos los eventos pasados hasta el dia de hoy

//si hay fecha final usar esa variable con un if
$sql = "SELECT * FROM eventos where mes_evento = '$mes_actual' AND dia_evento < '$dia_actual' ";
  $result = $db->query($sql);
    if ($result->num_rows > 0) { 
       while($row = $result->fetch_assoc()) { 

          if (!empty($row["ultimo_mes"])) {

           
          }else{
           // $sql = "DELETE FROM eventos where id =  ";
            //$resultado = $db->query($sql);
           // echo $row["id"];
          }

       }
    };
?>

  <h1 style="text-transform: uppercase;" > <?php echo $mes_actual; ?></h1>
    <div class="uk-section">
        <div class="owl-carousel owl-theme">
          
          <?php
          $sql = "SELECT * FROM eventos where mes_evento = '$mes_actual' ORDER BY dia_evento ASC ";
            $result = $db->query($sql);
              if ($result->num_rows > 0) {
                       echo '';
                        // output data of each row
                        while($row = $result->fetch_assoc()) {
                          if ($row["mes_evento"]== "Enero") {
                              $mes = "Ene";
                            } elseif ($row["mes_evento"] == "Febrero") {
                              $mes = "Feb";
                            } elseif ($row["mes_evento"] == "Marzo") {
                              $mes = "Mar";
                            } elseif ($row["mes_evento"] == "Abril") {
                              $mes = "Abr";
                            } elseif ($row["mes_evento"] == "Mayo") {
                              $mes = "may";
                            } elseif ($row["mes_evento"] == "Junio") {
                              $mes = "Jun";
                            } elseif ($row["mes_evento"] == "Julio") {
                              $mes = "Jul";
                            } elseif ($row["mes_evento"] == "Agosto") {
                              $mes = "Ago";
                            } elseif ($row["mes_evento"] == "Septiembre") {
                              $mes = "Sep";
                            } elseif ($row["mes_evento"] == "Octubre") {
                             $mes = "Oct";
                            } elseif ($row["mes_evento"] == "Noviembre") {
                             $mes = "Nov";
                            } elseif ($row["mes_evento"] == "Diciembre") {
                              $mes = "Dic";

                            }
                         
                         

                         echo'  <div class="item"> 
                            <a href="evento/?id='.$row["id"].'">
                            <img style="width:260px; height:260px;" src="' . $row["vistaprevia"].'">
                            </a>
                             <div class="informacion">
                               <div class="row">
                                  <div class="col-sm titulo-tarjeta" style="text-transform: uppercase;" ><p>'
                                   
                                   . $row["titulo"].
                                   
                           '</p>  </div>
                                  <div class="col-sm fecha-tarjeta">'
                                    .$row["dia_evento"].' <span style="text-transform: uppercase;" >'.$mes.'</span> '.$row["año_evento"].'
                                  </div>
                              </div>
                               <a class="ver-mas" href="evento/?id='.$row["id"].'">Ver más...</a>
                             </div>     
                          </div>';
  
                        }
                        
                    } else {
                       
                    }
                    
                    ?>
        </div>
    </div>



<h1 style="margin-top: -10px;" > EVENTOS DESTACADOS</h1>

    <div class="uk-section">
        <div class="owl-carousel owl-theme">



             <?php
          $sql = "SELECT * FROM eventos where destacado = 1 order by mes_evento asc, dia_evento asc  ";
            $result = $db->query($sql);
              if ($result->num_rows > 0) {
                       
                        // output data of each row
                        while($row = $result->fetch_assoc()) {
                          if ($row["mes_evento"]== "Enero") {
                              $mes = "Ene";
                            } elseif ($row["mes_evento"] == "Febrero") {
                              $mes = "Feb";
                            } elseif ($row["mes_evento"] == "Marzo") {
                              $mes = "Mar";
                            } elseif ($row["mes_evento"] == "Abril") {
                              $mes = "Abr";
                            } elseif ($row["mes_evento"] == "Mayo") {
                              $mes = "may";
                            } elseif ($row["mes_evento"] == "Junio") {
                              $mes = "Jun";
                            } elseif ($row["mes_evento"] == "Julio") {
                              $mes = "Jul";
                            } elseif ($row["mes_evento"] == "Agosto") {
                              $mes = "Ago";
                            } elseif ($row["mes_evento"] == "Septiembre") {
                              $mes = "Sep";
                            } elseif ($row["mes_evento"] == "Octubre") {
                             $mes = "Oct";
                            } elseif ($row["mes_evento"] == "Noviembre") {
                             $mes = "Nov";
                            } elseif ($row["mes_evento"] == "Diciembre") {
                              $mes = "Dic";

                            }
                         
                         

                         echo'  <div class="item"> 
                          <a href="evento/?id='.$row["id"].'">
                            <img style="width:260px; height:260px;" src="' . $row["vistaprevia"].'">
                            </a>
                             <div class="informacion">
                               <div class="row">
                                  <div class="col-sm titulo-tarjeta" style="text-transform: uppercase;" >'
                                   
                                   . $row["titulo"].
                                   
                           '        </div>
                                  <div class="col-sm fecha-tarjeta"style="text-transform: uppercase;">'
                                    .$row["dia_evento"].'<span> '.$mes.' </span>'.$row["año_evento"].'
                                  </div>
                              </div>
                               <a class="ver-mas" href="evento/?id='.$row["id"].'">Ver más...</a>
                             </div>     
                          </div>';




                          
                        }
                        
                    } else {
                       
                    }
                    $db->close();
                    ?>




        </div>
    </div>





   </div>
</div>


<div class="footer" >
      <div class="row info">
        <div class=" col-md-2 separador">
        </div>
        <div class=" col-md-2 logos">
            <div class="row">
              <img style="width: 160px;" src="img/logo.png">
            </div>
            <div class="row">
             <a href="https://oiledesign.com/" target="_blank"> <img style="width: 150px;" src="img/logo-oile.png"></a>
            </div>
        </div>
        <div class=" col-md-2">
            <div class="row">
                <div class=" redes">
                    <a href="https://www.facebook.com/OileArtDesign/" target="_blank" title="Facebook">
                      <i class="fab fa-facebook-f"></i></a>
               </div>
                <div class=" redes">
                    <a href="https://www.instagram.com/oile_py/" target="_blank" title="Facebook">
                      <i class="fab fa-instagram"></i></a>
               </div>
               <div class="redes">
                  <a href="mailto:contacto@voy.com.py" target="_blank" title="Facebook">
                  <i class="far fa-envelope"></i></a>
             </div>
            </div>
        </div>

        <div class=" col-md-2 info-footer">
          <h4>Direccion</h4>

          <p>López Moreira c/ R. Colmán <br>
          Asunción</p>
          <h4>Horarios</h4>
         <p> Lunes a Viernes: 11:00 - 19:00<br>
          Sábados:         10:00 - 18:00<br>
          Domingos:        Cerrado</p>
          <h4>Teléfono:</h4>
          <p>0981 400774</p>
          <h4>Mail:</h4>
          <p>contacto@voy.com.py</p>
        </div> 
      </div>      
</div>

</div>
<script>
window.onscroll = function() {myFunction()};

var header = document.getElementById("myHeader");
var sticky = header.offsetTop;

function myFunction() {
  if (window.pageYOffset > sticky) {
    header.classList.add("sticky");
  } else {
    header.classList.remove("sticky");
  }
}
</script>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js'></script>   
    <script src='https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.1/owl.carousel.js'></script>
    <script src="js/slider.js"></script>


<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
</body>
</html>
